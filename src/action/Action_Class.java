package action;

import java.io.File;
import java.io.IOException;
import java.util.logging.FileHandler;

import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.server.handler.GetScreenOrientation;



public class Action_Class {
	static WebDriver driver;
	

	public static void main(String[] args) throws Exception {
		
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\Admin\\Downloads\\Chromedriver\\chromedriver.exe");
		
		driver= new ChromeDriver();
		driver.get("https://www.guru99.com/keyboard-mouse-events-files-webdriver.html");
		Thread.sleep(2000);
		
		Actions actions = new Actions(driver);
		actions.moveToElement(driver.findElement(By.xpath("//*[@id=\"menu-5760-particle\"]/nav/ul/li[3]/div"))).build().perform();
		Thread.sleep(2000);
		actions.moveToElement(driver.findElement(By.xpath("//*[@id=\"menu-5760-particle\"]/nav/ul/li[3]/ul/li/div/div[2]/ul/li[2]/a"))).click().build().perform();
		Thread.sleep(2000);
		Takescreenshot("take_shot");
	
	}

	private static void Takescreenshot(String filename) throws IOException {
		File screen=((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		File d = new File("E:\\"+"filename+.jpg");
		org.openqa.selenium.io.FileHandler.copy(screen, d);
		
		System.out.println("Done");
		
	}}
	
		
		/*Actions actions = new Actions(driver);
		actions.moveToElement(driver.findElement(By.xpath("/html/body/header/nav/div[3]/ul/li[3]/a"))).build().perform();
		Thread.sleep(3000);
		actions.moveToElement(driver.findElement(By.id("//a[@id='submenu-lnk']"))).build().perform();
		Thread.sleep(3000);
		WebElement subelement=(driver.findElement(By.xpath("/html/body/header/nav/div[3]/ul/li[3]/ul/li[4]/ul/li[1]/a")));
		actions.moveToElement(subelement).click().build().perform();*/